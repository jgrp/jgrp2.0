define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone, Handlebars){

    var WebsiteView = Backbone.View.extend({
        el: '#content',
        events: {
            'click .webUrlButton': function(e) {
                 var url = e.target.dataset.link;
                window.open(url,'_blank');
            }
        },
        firstRow: {
          item: [
              {
                  title: 'WeidSide',
                  subTitle: 'Design und Entwicklung',
                  class: 'webWeid',
                  design: 'CSS3, Bootstrap',
                  develop: 'Typo3 6.2',
                  release: 'September 2014',
                  url: 'http://www.weidside.de/'

              },
              {
                title: 'jgrp.de',
                subTitle: 'this (portfolio website)',
                class: 'webJgrp',
                design: 'CSS3, Bootstrap',
                develop: 'Backbone.js, jQuery, Bootstrap, handlbars',
                release: 'Januar 2014',
                url: 'http://www.jgrp.de'

            }

          ]},
        secondRow: {
            item: [
                {
                    title: 'Toskana Umbrien Landhaus',
                    class: 'webToskana',
                    develop: 'PHP, HTML, CSS',
                    release: 'Februar 2012',
                    url: 'http://www.toskana-umbrien-landhaus.com/'

                },
                {
                  title: 'virtueller Rundgang',
                  subTitle: 'erstellt mit PanoramaStudio Pro 2',
                  class: 'webVR',
                  design: 'Photoshop',
                  develop: 'PHP, HTML, CSS',
                  release: 'Januar 2012',
                  url: 'http://www.mariae-himmelfahrt-friedberg.de/VirtuellerRundgang/index.php'

              }

            ]
        },
        thirdRow: {
            item: [
                {
                    title: 'Hotel Cornelia',
                    class: 'webCornelia',
                    design: 'Photoshop',
                    develop: 'HTML, CSS',
                    release: 'März 2010',
                    url: 'http://www.hotel-cornelia.de/'

                },
                {
                    title: 'fh map',
                    subTitle: 'Studienprojekt',
                    class: 'webFhMap',
                    design: 'Photoshop',
                    develop: 'HTML, CSS, XML, XSLT',
                    release: 'September 2009',
                    url: 'http://www.jgrp.de/websites/fh-map/start.html'

                }
            ]
        },
        template: {
            item:
                    '{{#each item}}\
                    <div class="col-sm-6">\
                        <div class="websiteItem">\
                            <div class="{{class}} websiteFrame"></div>\
                            <div class="websiteText">\
                               <div class="col-md-12"><h3>{{title}}</h3></div>\
                               <div class="col-md-12 subTitle"><p>{{subTitle}}{{#unless subTitle}}&nbsp;{{/unless}}</p></div>\
                               {{#if design}}\
                               <div class="col-xs-4">\
                                   <strong>Design</strong><br /> \
                               {{design}}\
                                </div>\
                                {{/if}}\
                                <div class="col-xs-4">\
                                   <strong>Entwicklung</strong><br />\
                                   {{develop}}\
                                </div>\
                                <div class="col-xs-4">\
                                   <strong>Release</strong><br />\
                                   {{release}}\
                                </div>\
                                 <div class="clearfix"></div>\
                                <div class="webUrlButton" data-link="{{url}}">\
                                    zur Website\
                                 </div>\
                            </div>\
                        </div>\
                    </div>\
                    {{/each}}'
        },
        initialize: function() {
            //console.log("init");
            window.onresize = this.resize;
        },
        render: function() {
            this.webContent = Handlebars.compile(this.template.item);
            this.$el.html('<div class="row">' + this.webContent(this.firstRow)+ '</div>');
            this.$el.append('<div class="row">' + this.webContent(this.secondRow)+ '</div>');
            this.$el.append('<div class="row">' + this.webContent(this.thirdRow)+ '</div>');
            $("#siteHeadline").text('websites');
            this.resize();
        },
        resize: function() {
            console.log("resize");
            $(".websiteFrame").each(function () {
                console.log($(this).width());
                $(this).height($(this).width() / 2.35);
            });
        }
    });

    return WebsiteView;
});