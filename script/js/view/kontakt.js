define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone, Handlebars){

    var KontaktView = Backbone.View.extend({
        el: '#content',
        events: {
            'click #formSubmitBtn' : function(e) {
                $("#contactMsg").html('');
                var btn = $(e.currentTarget);
                btn.button('loading');
                $("#panelContact .panel-body").removeClass("alert-danger");

                $.ajax({
                    url: 'script/php/mailing.php',
                    type: 'POST',
                    data: {
                        name :   $("#inputName").val() ,
                        email:   $("#inputEmail").val(),
                        msg:     $("#txtMsg").val(),
                        captcha: $("#inputCaptcha").val()
                    },
                    success:function(response) {
                        console.log(response);
                        var response = JSON.parse(response);
                        btn.button('reset');
                        if (response.state === 'error') {

                            $("#panelContact .panel-body").addClass("alert-danger");

//                            class="has-error"

//                            $.each(response.errorStack, function(key, value))
//
//                            switch (value)  {
//                                case 'name' :
//                                case 'email' :
//                                case 'msg' :
//                                case 'captcha' :
//                            }

                            $("#contactMsg").html('<span style="color: #A94442">Achtung, da stimmt wohl etwas nicht...</span>');

                        }
                        else {
                            $("#contactMsg").html('<span style="color: #006699">Viele Dank für Ihre Nachricht. Ich freue mich immer, wenn mir jemand schreibt ;)</span>');
                        }
                    },
                    error: function(error) {
                        console.log("error");
                        console.log(error);
                    }
                })

            }
        },
        template: {
            content:
                '<div class="row">\
                        <div class="col-md-6">\
                            <div class="panel panel-default" id="panelContact">\
                                <div class="panel-heading">Kontaktformular</div>\
                                <div class="panel-body">\
                                <form role="form">\
                                    <div class="form-group">\
                                        <!--<label for="name">Name</label> -->\
                                        <input type="text" class="form-control" id="inputName" placeholder="Name">\
                                    </div>\
                                    <div class="form-group">\
                                        <!--<label for="exampleInputEmail1">Email address</label>-->\
                                        <input type="email" class="form-control" id="inputEmail" placeholder="Email">\
                                    </div>\
                                    <div class="form-group">\
                                        <textarea class="form-control" rows="3" id="txtMsg" placeholder="Nachricht"></textarea>\
                                    </div>\
                                    <div class="form-group">\
                                        <div class="input-group">\
                                            <span class="input-group-addon">20 + 85 = </span>\
                                            <input type="text" id="inputCaptcha" class="form-control" placeholder="Ergebnis">\
                                        </div>\
                                    </div>\
                                    <button type="submit" id="formSubmitBtn" class="btn btn-default btn-block" data-loading-text="Moment... Ich prüfe das">Abschicken</button>\
                                </form>\
                                <div class="help-block" id="contactMsg"></div>\
                                </div>\
                            </div>\
                        </div>\
                        \
                         <div class="col-md-6">\
                            <div class="panel panel-default">\
                                <div class="panel-heading">Impressum</div>\
                                <div class="panel-body">\
                                    <h2>seiteninhaber</h2>\
                                     <img src="/address.jpg" alt="Adresse" />\
                                     <h2>copyright</h2>\
                                     Alle Veröffentlichtungen sind Eigentum von Johannes Grupp. Jede weitere Nutzung ist untersagt.\
                                     <h2>verwendet für diese seite</h2>\
                                     jQuery <br />\
                                     Backbone.js <br />\
                                     underscore.js <br />\
                                     requirejs <br />\
                                     handlebars <br />\
                                     Bootstrap <br />\
                                     google webfonts <br />\
                                     IcoMoon <br />\
                                     <h2>partnersites</h2>\
                                     <a href="http://www.coopdb.de" target="_blank">coopdb.de | Oliver Fuchs</a><br />\
                                     <a href="http://www.steinm.com/" target="_blank">steinm.com | Matthias Stein</a><br />\
                                </div>\
                            </div>\
                        </div>\
                    </div>'
        },
        initialize: function() {
        },
        render: function() {
            this.$el.html(this.template.content);
            $("#siteHeadline").text('kontakt und impressum');
        }
    });

    return KontaktView;
});