define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone, Handlebars){

    var videosView = Backbone.View.extend({
        el: '#content',
        events: {
        },
        template: {
            content:
                   '<div class="row videoItem">\
                        <div class="col-md-8 videoContainer"> \
                            <iframe width="100%" class="videoFrame" src="http://www.youtube.com/embed/nLAbZUHg5ik?rel=0&amp;hd=1" frameborder="0"></iframe>\
                        </div>\
                        <div class="col-md-4 videoText">\
                            <div>\
                                <h2>real life</h2>\
                                "Werbeclip" - Studienprojekt\
                                 <p>\
                                    Kamera und Schnitt<br />\
                                    Adobe Premiere\
                                </p>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="row videoItem">\
                        <div class="col-md-8 videoContainer">\
                        <iframe width="100%" class="videoFrame" src="http://www.youtube.com/embed/FCX8Cv0TOPs?rel=0&amp;hd=1" frameborder="0"></iframe>\
                        </div>\
                        <div class="col-md-4 videoText">\
                             <div>\
                                <h2>geisterstunde</h2>\
                                Musikvideo  \
                                 <p>\
                                    2. Kamera, Schnitt, Farbkorretkur <br />\
                                    Adobe Premiere, Adobe After Effects\
                                </p>\
                            </div>\
                       </div>\
                   </div>'
        },
        initialize: function() {
            window.onresize = this.resize;

        },
        render: function() {
            this.$el.html(this.template.content);
            $("#siteHeadline").text('videos');
            this.resize();
        },
        resize: function() {
            console.log("videos resize");
            $(".videoFrame").each(function () {
                $(this).height($(this).width() / 1.77);
            });
        }
    });

    return videosView;
});