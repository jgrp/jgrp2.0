define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone, Handlebars){

    var DreiDView = Backbone.View.extend({
        el: '#content',
        events: {
//            'click .image': function(e) {
//                window.open(e.currentTarget.attributes['data-link'].nodeValue);
//            }
        },
        template: {
            content:
                '<div class="row videoItem">\
                        <div class="col-md-8 videoContainer"> \
                            <div class="dreiDFrame imgKawasaki">\
                            <div class="imgkawa1"></div>\
                            <div class="imgkawa2 imgkawa"></div>\
                            <div class="imgkawa3 imgkawa"></div>\
                            </div>\
                        </div>\
                        <div class="col-md-4 videoText">\
                            <div>\
                                <h2>kawasaki ninja</h2>\
                            <p> Studienprojekt</p>\
                            </div>\
                        </div>\
                    </div>\
                        <div class="row videoItem">\
                            <div class="col-md-8 videoContainer">\
                                <div class="imgKerze dreiDFrame"></div>\
                            </div>\
                            <div class="col-md-4 videoText">\
                                <div>\
                                    <h2>kerze</h2>\
                               <p> Studienübung  </p>\
                                </div>\
                            </div>\
                        </div>'
        },
        initialize: function() {
            window.onresize = this.resize;
        },
        render: function() {
            this.$el.html(this.template.content);
            $("#siteHeadline").text('3D modellierung mit maya');
            this.resize();
        },
        resize: function() {
            console.log("dreid resize");
            $(".dreiDFrame").each(function () {
                $(this).height($(this).width() / 1.77);
            });

            $(".imgkawa").each( function() {
                $(this).width($(this).height());
            });


            $(".imgkawa1").width($(".videoContainer").width() -  $(".imgkawa2").width());

        }
    });

    return DreiDView;
});