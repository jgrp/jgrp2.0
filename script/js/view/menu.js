define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'router',
    'bootstrap'
], function($, _, Backbone, Handlebars, Router){

    var MenuView = Backbone.View.extend({
        el: '',
        events: {
//            'hover .menuIcon': function(e) {
//                    $(".menuPopout." + $(this).data("link")).css({top: $(this).offset().top});
//            },
            'click .menuButton': function(e) {
                console.log(e);
                var title = '';
                var offset = '';

            },
            'mouseenter #menu .menuButton': function(e) {
                console.log(e);
            },
            'mouseleave #menu .menuButton': function(e) {
                console.log(e);
            }

        },
        menuItems : {
            menuItem: [
                {
                    icon: 'screen' ,
                    route: 'websites',
                    active : 'active'
                },
                {
                    icon: 'file' ,
                    route: 'design'
                },
                {
                    icon: 'camera' ,
                    route: 'fotografie'
                },
                {
                    icon: 'play2' ,
                    route: 'videos'
                },
                {
                    route: 'dreid',
                    label: '3D',
                    text : 'modellierung'
                },
                {
                    icon: 'envelope' ,
                    route: 'kontakt'
                }
            ]
        },
        template: {
            menuBig : '\
                {{#each menuItem}} \
                 <li><div class="{{#if icon}}icon-{{icon}}{{/if}} menuButton {{active}} {{route}}" data-route="{{route}}" title="{{#if text}}{{text}}{{else}}{{route}}{{/if}}"><span class="menuLabel">{{label}}</span></div><span class="bigMenuTitle" id="bigMenuTitle{{route}}">{{#if text}}{{text}}{{else}}{{route}}{{/if}}</span></li> \
                {{/each}}',

            menuSmall: '\
                {{#each menuItem}} \
                 <div class="col-xs-6"><div class="menuButton {{route}}" data-route="{{route}}"><span class="{{#if icon}}icon-{{icon}}{{/if}} icon"></span> {{label}} {{#if text}}{{text}}{{/if}} {{#unless text}}{{route}}{{/unless}}</div></div> \
                {{/each}}'

        },
        initialize: function() {
        },
        bindEvents: function() {
            var _menu = this ;
            $("body").on('click', '.menuButton' ,function() {
                var route = $(this).data('route');
                Router.router.navigate(route, {trigger: true});
                _menu.setActive(route);
            });

            $(".menuButton").hover(
                function() {
                    var id = "#bigMenuTitle" + $(this).data('route');
                    $(id).addClass("show");
//                    $(id).fadeIn(500);
                }, function() {
                    var id = "#bigMenuTitle" + $(this).data('route');
//                    $(id).fadeOut(500);
                    $(id).removeClass("show");
                }
            );
        },
        render: function() {
            var _menu = this;

            var tplMenuBig = Handlebars.compile(this.template.menuBig);
            var tplmenuSmall = Handlebars.compile(this.template.menuSmall);
            $("#menuIcons").html(tplMenuBig(this.menuItems));
            $("#menuSmall").html(tplmenuSmall(this.menuItems));

//            $("#menuIcons div").tooltip({
//                placement: 'right',
//                container: 'body'
//
//            });

            this.bindEvents();
        },
        setActive: function(route) {
            $(".menuButton").removeClass('active')
            $(".menuButton." + route).addClass("active");
        }

    });

    return MenuView;
});