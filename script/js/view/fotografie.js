﻿define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone, Handlebars){

    var FotoView = Backbone.View.extend({
        el: '#content',
        events: {
            'click .image': function(e) {
                window.open(e.currentTarget.attributes['data-link'].nodeValue);
            }
        },
        row1: {
            categories: [
                {
                    name: 'gedanken'
                },
                {
                    name: 'licht'
                }
            ]},
        row2: {
            categories: [    {
                    name: 'unzertrennlich'
                },
                {
                    name: 'wasser'
                } ]},
        row3: {
            categories: [
                {
                    name: 'blau'
                },
                {
                    title: 'ganz in weiss',
                    name: 'weiss'
                } ]},
        row4: {
            categories: [
                {
                    name: 'fenster'
                }

            ]
        },
        template: {
            content:
                '{{#each categories}} \
                <div class="col-sm-6">\
                <h2 class="imgHead">{{#if title}}{{title}}{{else}}{{name}}{{/if}}</h2> \
                    <div class="imgCont {{name}}" data-album="{{name}}">lade Bilder...</div>\
                    </div>\
                    {{/each}}',
            footer: '<div class="clearfix"></div>\
                    <div id="footer" class="row">\
                        <div class="col-xs-12">Copyright: Alle hier veröffentlichten Bilder sind Eigentum von Johannes Grupp. Jede weitere Nutzung der Bilder ist untersagt.\
                    </div>\
                           ',
            bilderListe: '<ul class="bilderListe">\
                                 {{#each bilderliste}}\
                                    <li class="image" style="background-image: url({{pfadKlein}})" data-link="{{dateipfad}}"></li>\
                                 {{/each}}\
                                 </ul>'
        },
        initialize: function() {
        },
        render: function() {
            var contTemplate = Handlebars.compile(this.template.content);
            this.$el.html('<div class="row">' + contTemplate(this.row1)+ '</div>');
            this.$el.append('<div class="row">' + contTemplate(this.row2)+ '</div>');
            this.$el.append('<div class="row">' + contTemplate(this.row3)+ '</div>');
            this.$el.append('<div class="row">' + contTemplate(this.row4)+ '</div>');
            this.$el.append(this.template.footer);

            $("#siteHeadline").text('fotografie');
            this.loadImages();
        },
        loadImages: function() {
            var imgTemplate = Handlebars.compile(this.template.bilderListe);

            $(".imgCont").each(function() {
                var name = $(this).data('album');

                $.ajax({
                    type: "POST",
                    url: 'script/php/bilder.php',
                    data: {
                        album: name
                    },
                    success: function(data){
                        var bilder = $.parseJSON(data);
                        $(".imgCont." + name).html(imgTemplate(bilder));
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            });

        }
    });

    return FotoView;
});