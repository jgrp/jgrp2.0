define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone, Handlebars){

    var DesignView = Backbone.View.extend({
        el: '#content',
        events: {
//            'click .image': function(e) {
//                window.open(e.currentTarget.attributes['data-link'].nodeValue);
//            }
        },
        template: {
            content:
                '<div id="printDesign">\
                    <div class="row printRow">\
                        <div class="col-md-6">\
                            <h2>hochzeitskarten</h2>\
                            <div class="row">\
                               <div class="col-md-6"><img src="gestaltung/hoch1.jpg" class="img-responsive" /></div>\
                                <div class="col-md-6"><img src="gestaltung/hoch2.jpg" class="img-responsive" /></div>\
                            </div>\
                        </div>\
                        <div class="col-md-5 col-md-offset-1">\
                         <h2>flyer</h2>\
                            <div class="row">\
                               <div class="col-md-8"><img src="gestaltung/neuzugezogen2.jpg" class="img-responsive" /></div>\
                                <div class="col-md-4"><img src="gestaltung/Jugendfahrt_poster.jpg" class="img-responsive" /></div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="row printRow">\
                        <div class="col-md-5">\
                             <h2>broschüre</h2>\
                             <div class="row">\
                                <div class="col-xs-4"><img src="gestaltung/cover2b.jpg" class="img-responsive" /></div>\
                                <div class="col-xs-8"><img src="gestaltung/Pfeiler.jpg" class="img-responsive" /></div>\
                            </div>\
                        </div>\
                        <div class="col-md-6 col-md-offset-1">\
                            <h2>logodesign</h2>\
                             <div class="row">\
                                <div class="col-xs-6"><img src="gestaltung/logo-jo-m.jpg" class="img-responsive" /></div>\
                                <div class="col-xs-6"><img src="gestaltung/logo2.jpg" class="img-responsive" /></div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="row printRow">\
                        <div class="col-md-6">\
                            <h2>weinetikett</h2>\
                            <div class="row">\
                                <div class="col-xs-6"><img src="gestaltung/wein.jpg" class="img-responsive" /></div>\
                                <div class="col-xs-6"><img src="gestaltung/wein2.jpg" class="img-responsive" /></div>\
                            </div>\
                        </div>\
                    </div>\
                </div>'
        },
        initialize: function() {
        },
        bindEvents: function() {
            $("#printDesign img").click(function () {
                var src = $(this).attr("src");
                window.open(src,'_blank');

            });
        },
        render: function() {
            this.$el.html(this.template.content);
            $("#siteHeadline").text('print design');
            this.bindEvents();
        }

    });

    return DesignView;
});