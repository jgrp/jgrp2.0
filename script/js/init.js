require.config({
    baseUrl: 'script/js',
    paths: {
        jquery:     'lib/jquery-1.10.2.min',
        backbone:   'lib/backbone-min',
        bootstrap:  'lib/bootstrap.min',
        handlebars: 'lib/handlebars-v1.1.2',
        underscore: 'lib/underscore'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        handlebars: {
            exports: 'Handlebars'
        },
        jquery: {
            exports: '$'
        },
        backbone: {
            deps: ['underscore','jquery'],
            exports: 'Backbone'
        },
        bootstrap: {
            deps: ['jquery']
        }
    }
});

require(['app'], function(App){
    // The "app" dependency is passed in as "App"
    App.initialize();
});