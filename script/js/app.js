define([
    'jquery',
    'underscore',
    'backbone',
    'router',
    'view/menu'
], function($, _, Backbone, Router, Menu){
    var initialize = function(){
        // Pass in our Router module and call it's initialize function
        Router.initialize();

        var menu = new Menu();
        menu.render();


    }

    return {
        initialize: initialize
    };
});