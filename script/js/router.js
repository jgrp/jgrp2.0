define([
    'jquery',
    'underscore',
    'backbone',
    'view/fotografie',
    'view/websites',
    'view/design',
    'view/videos',
    'view/dreid',
    'view/kontakt'
], function($, _, Backbone, FotoView, WebsitesView, DesignView, VideosView, DreidView, KontaktView){
    var fotoView = new FotoView();
    var websitesView = new WebsitesView();
    var designView = new DesignView();
    var videosView = new VideosView();
    var dreidView = new DreidView();
    var kontaktView = new KontaktView();
    var Router = Backbone.Router.extend({
        routes: {
            // Define some URL routes
            '': 'websites',
            'fotografie': 'fotografie',
            'websites': 'websites',
            'design': 'design',
            'videos': 'videos',
            'dreid': 'dreid',
            'kontakt': 'kontakt'

            // Default
//            '*actions': 'defaultAction'
        },
        fotografie: function() {
            fotoView.render();
        },
        websites: function() {
            websitesView.render();
        },
        design: function() {
            designView.render();
        },
        videos: function() {
            videosView.render();
        },
        dreid: function() {
            dreidView.render();
        },
        kontakt: function() {
            kontaktView.render();
        }
    });

    var initialize = function(){
        this.router = new Router;
        Backbone.history.start();
    };
    return {
        initialize: initialize,
        router: this.router
    };
});