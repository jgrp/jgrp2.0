
<?php
ini_set('exif.encode_unicode', 'UTF-8');
$album = $_POST['album'];

$dir =  '../../bilder/' . $album ;

$dateiliste = scandir($dir);

$i = 0;
$bilder = array('title'=> $album , 'bilderliste' => array());

while ($i < count($dateiliste)) {

    $file = $dateiliste[$i];
    $path_parts = pathinfo($file);
    $dateipfad = $dir . '/' . $file;

    if ( is_file($dateipfad) && ( ($path_parts['extension'] == 'png') or ($path_parts['extension'] == 'jpg') or ($path_parts['extension'] == 'JPG') ) ) {
        $filename = $path_parts['filename'];
        $basename = $path_parts['basename'];
//        $exif = exif_read_data($dateipfad, 'EXIF');
//        $beschreibung =  htmlentities($exif["ImageDescription"]);
        $image = getimagesize($dateipfad);

        array_push($bilder['bilderliste'],
            array(
                'dateipfad' => 'bilder/' . $album . '/' . $basename ,
                'pfadKlein' => 'bilder/' . $album . '/klein/' . $basename ,
                'filenmame' => $filename ,
                'basename' => $basename ,
//                'beschreibung' => $beschreibung
            ) );
    }
    $i++;
}

echo json_encode($bilder);

?>



