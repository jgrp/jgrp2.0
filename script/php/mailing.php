<?php

######################## CREATE TIMESTAMPS ####################################################

$timestamp = "Eingegangen am ".date('d.m.Y')." um ".date('H:i')." Uhr.";
$year = date('Y'); //used for current COPYRIGHT STAMP



/* ####################################################################################################################################  */
/* ####################################################################################################################################  */
/* ####################################################################################################################################  */
/* ############################################### +-----------------------------+ ####################################################  */
/* ############################################### | BEGIN VARIABLES DECLARATION | ####################################################  */
/* ############################################### +-----------------------------+ ####################################################  */
/* ####################################################################################################################################  */
/* ####################################################################################################################################  */
/* ####################################################################################################################################  */

//FIELDS
$name    = $_POST['name'];
$email   = $_POST['email'];
$msg     = $_POST['msg'];
$captcha = $_POST['captcha'];

//OFTEN USED ERROR MESSAGES
$error	      = "none";
$error_msg 	  = "Bitte &uuml;berpr&uuml;fen Sie Ihre Eingaben!";

//REGULAR EXPRESSIONS
$name_muster   = "/^[a-zA-Z \ä\&ouml;\ü\Ä\Ü\Ö\ß_-\´\`]+$/ism";
$email_muster  = "/^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\.[a-zA-Z]{2,4}$/";
$date_d_muster = "/^[0-9]{1,2}$/";
$date_m_muster = "/^[0-9]{1,2}$/";
$date_y_muster = "/^[0-9]{4}$/";

//CHECK_SUMS: 0=false | 1=true --> (DEFAULT=0)
$check_name       = '0';
$check_email  	  = '0';
$check_msg        = '0';
$check_captcha    = '0';



//MAIL RECIPIENT
$recipient = "mail@jgrp.de";

//MAIL SUBJECT
$subject = "Nachricht von jgrp.de";

//MAIL DISPLAY SUCCESS AFTER SEND
$sent_success = "none";


######################## MAIL HEADER (STANDARD) ###############################################

$header =  "MIME-Version: 1.0\n";
    $header .= "Content-type: text/html; charset=utf-8\n";
    $header .= "From: $email <$email>\n";
    $header .= "Reply-To: $email <$email> \n"; 
    $header .= "X-Mailer: PHP/" . phpversion() . "\n";
//    $header .= "X-Sender-IP: " . $REMOTE_ADDR . "\n";
    $header .= "Return-Receipt-To:".$recipient."<".$recipient.">\n";
    /*$Header .= "Envelope-To:".$email."<".$email.">\n";
    $Header .= "Disposition-Notification-To:".$email."<".$email.">\n";// disposition confirmation
    $Header .= "X-Confirm-Reading-To: $sender\n";// reading confirmation */


######################## MAIL CONTENT (HTML) ##################################################

$mail = "	<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
			<head>
				<title>Nachricht von jgrp.de</title>
				<meta http-equiv='content-type' content='text/html;charset=utf-8' />
			</head>
			<body>
			<table style='font-size:13px;' border='0' cellpadding='0' width='700' >
				<tr>
					<td colspan='2' style='font-weight:bold;font-size:14px;'>Nachricht von jgrp.de</td>
				</tr>
				<tr>
					<td width='150'>&nbsp;</td>
					<td width='500'>&nbsp;</td>
				</tr>
				<tr>
					<td colspan='2'>".$timestamp."</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Name:</td>
					<td>".$name."</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>

					<td>E-Mail:</td>
					<td><a href='mailto:".$email."'>".$email."</a></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td valgin='top'>Nachricht:</td>
					<td>".$msg."</td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan='2'><hr size='1' /></td>
				</tr>
				<tr>
					<td colspan='2' style='font-size:12px;'>&nbsp;<a href='#' target='_blank'>&copy; ".$year." &middot; http://www.jgrp.de/</a></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan='2' style='font-size:9px;'>Hinweis: Die Versendung der E-Mail erfolgt mit Zeichencodierung UTF-8. <br/>Wenn bei der Darstellung der Zeichen insbesondere der Umlaute Fehler angezeigt werden, stellen Sie bitte Ihr E-Mail-Programm auf eine universelle Zeichentabelle (Unicode) bzw. UTF-8-Code um!</td>
				</tr>
			</table>
			</body>
			</html>";
			


/* ####################################################################################################################################  */
/* ####################################################################################################################################  */
/* ####################################################################################################################################  */
/* ############################################### +-----------------------------+ ####################################################  */
/* ############################################### | END VARIABLES DECLARATION   | ####################################################  */
/* ############################################### +-----------------------------+ ####################################################  */
/* ####################################################################################################################################  */
/* ####################################################################################################################################  */
/* ####################################################################################################################################  */



######################## CHECK NAME ###########################################################
    $errorStack = array();


     if(empty($name)){
          $check_name = '0';
		  $error_name = "Name fehlt!";
          array_push($errorStack, 'name');
        }
        elseif(@preg_match($name_muster, $name) == 0){
          $color_name = $color_error;
          $check_name = '0';       
		  $error_name = "Name ist ung&uuml;ltig!";
            array_push($errorStack, 'name');
        }
        else{
          $check_name = '1';
        }

######################## CHECK EMAIL ###########################################################		

     if(empty($email)){
          $check_email = '0';
		  $error_email = "E-Mail fehlt!";
         array_push($errorStack, 'email');
        }
        elseif(@preg_match($email_muster, $email) == 0){
          $check_email = '0';
		  $error_email = "E-Mail ist ung&uuml;ltig!";
            array_push($errorStack, 'email');
        }
        else{
          $check_email = '1';
        }


######################## CHECK MESSAGE ################################################################

//     if(empty($msg)){
//          $color_msg = $color_error;
//          $check_msg = '0';
//		  $error_txt = "Nachricht fehlt!";
//		  $error_show = "error_desc_true";
//        }
//        elseif(@preg_match($msg_muster, $msg) == 0){
//          $color_msg = $color_error;
//          $check_msg = '0';
//		  $error_txt = strlen($msg)." von mind. 100 Zeichen!";
//		  $error_show = "error_desc_true";
//        }
//        else{
//          $check_msg = '1';
//		  $error_show = "error_desc_false";
//        }
//
       if(empty($msg)){
          $check_msg = '0';
		  $error_txt = "Ihre Nachricht enth&auml;lt nur ".strlen($msg)." von mind. 100 Zeichen!";
           array_push($errorStack, 'msg');
        }
        else{
          $check_msg = '1';
        }


######################## CHECK CAPTCHA ################################################################

        if($captcha != 105){
            $check_captcha = '0';
            $error_txt = "Falsches Captcha";
            array_push($errorStack, 'captcha');
        }
        else{
            $check_captcha = '1';
        }

######################## WHOLE END CHECK + MAIL SENDING + UNSET #########################################

      if( $check_name && 
	      $check_email &&
          $check_captcha
		  == '1' ){
			 
          @mail($recipient, $subject, $mail, $header);
          //header("Location: mail_success.html");

            echo json_encode(array('state' => 'success'));

          }
        else{
            echo json_encode(array('state' => 'error', 'errorStack' => $errorStack ));
          }


?>